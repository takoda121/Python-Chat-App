<h1 align="center">Python Chat App 💬</h1>  

[![Typing SVG](https://readme-typing-svg.demolab.com?font=Fira+Code&pause=1000&width=435&lines=Coded+in+PHP%2C+Python;Made+by+systemd_arch;Open+source+(GNU+GPLv3);Backward+Compatible+)](https://git.io/typing-svg)

### Make sure that the client and server are both from GitLab!

## Compiling 💽
> Tested with pyinstaller  
> Cant assist if its compiled

## Server 👨‍💻
> The server was tested with php -S   
> Requires "Chat Key" aka the chat is read only without a key  
> Stores: Message, Username (Extracted from Chat Key), Time the message was sent  
> With moving this project to Gitlab makes it not work with old versions!
### db.json (Server) 💾
> A json file that stores the messages, usernames, and time that the message was sent  
> Not encrypted (yet)
### users.json (Server) 💾
> Stores Chat Key, Name for the chat key user, Role for the chat key user  
> Current roles are user and admin
### Admin role 🚨
> It's not that hard! Just put this character: ㅤ (\u3164) next to their username!   
> Makes their username red   

